-- Markov: Haskell Powered Calculator
--Daniel Grinshpon
import System.IO
import System.Exit
import Data.Map (Map)
import qualified Data.Map as Map
import Markov

variableMap = Map.empty

initialize = do
    putStrLn "-----------------------------\n|-----Markov Calculator-----|\n-----------------------------"
    input variableMap

input :: Map String Double -> IO ()
input vars = do
    putStr "> "
    hFlush stdout
    inpt <- getLine
    let out = output inpt vars
    let outS = unpackFirst out
    putStrLn outS
    if outS == "Quit"
        then exitSuccess
    else input $ unpackMap out --eventually the previous 'out' will be able to be used as an ANS in a next expression

main = initialize

-- Markov: Haskell Powered Calculator
--Daniel Grinshpon
module Markov where
import Data.Map (Map)
import qualified Data.Map as Map

data Symbol = Add | Sub | Mult | Div | Pow | Equals deriving (Show,Eq)
data Container = Bracket | Brace | Paren | EndSub deriving (Show,Eq)
data Name = Quit | Save | Recalc deriving (Show, Eq)
data Id = Number Double | Matrix (Int,Int) [Double] | Variable String Double deriving(Show,Eq)
-- Variable String Id (for matrices)
-- Matrix WIP: will be a special type of list

data Token = Keysymbol Char | Identifier String | Statement String | Open Container | Close Container deriving (Show) -- value/number
-- integrals/derivatives/etx will be KEYWORDS
data Tree = Operator Symbol | Argument Tree | Value Id | Term [Tree] | Command Name deriving (Show) -- ??? Parser Tree not finalized

-- Here is the implementation that will allow Markov to output not only numbers, but structures such as matrices and vectors, and error messages
-- The math for matrix operations will be located here as well
data Output = Result Id | Message String
instance Show Output where
    show (Result (Number x)) = show x
    show (Result (Matrix (r,c) xs)) = show xs
    show (Message msg) = msg
instance Num Output where
    (Message x) + (Message y) = Message $ x++"\n"++y
    (Result (Number x)) + (Result (Number y)) = Result $ Number $ x+y
    (Result (Number x)) - (Result (Number y)) = Result $ Number $ x-y
    (Message x) - (Message y) = Message $ x++"\n"++y
    (Result (Number x)) * (Result (Number y)) = Result $ Number $ x*y
    (Message x) * (Message y) = Message $ x++"\n"++y
instance Fractional Output where
    (Message x) / (Message y) = Message $ x++"\n"++y
    (Result (Number x)) / (Result (Number 0)) = Message "Error: Division by zero"
    (Result (Number x)) / (Result (Number y)) = Result $ Number $ x/y
instance Floating Output where
    (Message x) ** (Message y) = Message $ x++"\n"++y
    (Result (Number x)) ** (Result (Number y)) = Result $ Number $ x**y


contains :: String -> String -> Bool
contains _ [] = False
contains [] _ = True
contains (m:ain) check = (m `elem` check) && (ain `contains` check)

--hasOne :: String -> Char -> Bool
--hasOne x c =

--toDouble :: String -> Double
--toDouble x   | x `hasOne` '.' = read x :: Double
            -- | otherwise = error "Parse Error: Double Error: Cannot Convert To Number"

toDouble :: String -> Double
toDouble x = read x :: Double --TODO: replace read with read maybe

unpackFirst :: (Eq a) => (a, Map String Double) -> a
unpackFirst (x,_) = x

unpackMap :: (Eq a) => (a, Map String Double) -> Map String Double
unpackMap (_,x) = x

lexer :: String -> [Token]
lexer [] = []
lexer (' ':str) = lexer str --ignore whitespace
--lexer (x:' ':str) = lexer (x:str)
lexer (':':s) = [Statement s]
lexer (d:str)   | d `elem` "^*/+-=" = (Keysymbol d):(lexer str)
                | d == '(' = (Open Paren):(lexer str)
                | d == '[' = (Open Brace):(lexer str)
                | d == '{' = (Open Bracket):(lexer str)
                | d == ')' = (Close Paren):(lexer str)
                | d == ']' = (Close Brace):(lexer str)
                | d == '}' = (Close Bracket):(lexer str)
                | otherwise = case lexer str of
    ((Identifier n):xs) -> (Identifier (d:n)):xs --Will need to differentiate fractions/decimals [5.5.5 returns error] (and variables [x,y,..])
    xs -> (Identifier [d]):xs

strip :: [Token] -> Int -> [Token]
strip tkns 0 = tkns
strip ((Open x):tkns) n = strip tkns (n+1)
strip ((Close x):tkns) n = strip tkns (n-1)
strip (t:ts) n = strip ts n

subParse :: [Token] -> [Tree]
subParse ((Close EndSub):_) = []
subParse tkns = (Term (parse tkns)):(parse (strip tkns 1))

parse :: [Token] -> [Tree]
parse [] = []
parse [Statement cmd]   | cmd == "q" = [Command Quit]
                        | cmd == "s" = [Command Save]
                        | cmd == "r" = [Command Recalc]
                        | otherwise = error "(62)Parse Error: Unkown Command"
parse ((Open _):tkns) = subParse tkns
parse ((Close _):tkns) = subParse ((Close EndSub):tkns)
parse ((Keysymbol d):tkns)  | d == '+' = (Operator Add):(parse tkns)
                            | d == '-' = (Operator Sub):(parse tkns)
                            | d == '*' = (Operator Mult):(parse tkns)
                            | d == '/' = (Operator Div):(parse tkns)
                            | d == '^' = (Operator Pow):(parse tkns)
                            | d == '=' = (Operator Equals):(parse tkns)
parse ((Identifier (x:xs)):tkns)    | (x:xs) `contains` "1234567890." = (Value (Number (toDouble (x:xs)))):(parse tkns)
                                    | not (x `elem` "1234567890.") = (Value (Variable (x:xs) 0.0)):(parse tkns) -- VARIABLE FUNCTIONALITY WIP
                                    | otherwise = error "(73)Parse Error: Invalid Identifier"
--parse ((Identifier x):tkns) = (Variable x 0):(parse tkns) -- NOT COMPLETE: PLACEHOLDER EVAL

-- write preEvaluate to determine whether something is an equation or function or command etc.
preEvaluate :: ([Tree], Map String Double) -> (String, Map String Double)
preEvaluate ([],v) = ("", v)
preEvaluate ([Command cmd],v) = (show cmd, v)
preEvaluate (x,varMap) =
    let evaluated = evaluate (x,varMap)
        evalString = show $ unpackFirst evaluated
        evalMap = unpackMap evaluated
    in (evalString,evalMap)
--preEvaluate (x,varMap) = (show $ evaluate (x,varMap),varMap)

interpret :: (Tree, Map String Double) -> Double
interpret ((Value (Number n)),_) = n
interpret ((Value (Variable x n)),varMap) = varMap Map.! x -- Variable functionality WIP
interpret ((Term t),varMap) = unpackFirst $ evaluate (t,varMap)
interpret (_,_) = error "(86)Interpret Error: Incomplete Input"

evaluate :: ([Tree], Map String Double) -> (Double, Map String Double) --TODO: Wrap tuples (replace tuples with state monad)
evaluate ([],v) = (0,v) -- should not be matched
evaluate ([t],varMap) = (interpret (t,varMap),varMap)
evaluate (((Value(Variable x val)):(Operator Equals):ts),varMap) = (eval_ts,(Map.insert x (eval_ts) varMap)) where eval_ts = unpackFirst $ evaluate (ts,varMap)
evaluate (((Operator op):t:ts),varMap)  | op == Add = evaluate ((Value(Number(0+(interpret (t,varMap))))):ts,varMap)
                                        | op == Sub = evaluate ((Value(Number(0-(interpret (t,varMap))))):ts,varMap)
                                        | otherwise = error "(94)Evaluate Error: Incomplete Input"
evaluate ((n:(Operator op):m:(Operator op1):l:ts),varMap)   | op1 == Mult = evaluate (n:(Operator op):(Value (Number ((interpret (m,varMap)) * (interpret (l,varMap))))):ts,varMap)
                                                            | op1 == Div = evaluate (n:(Operator op):(Value (Number ((interpret (m,varMap)) / (interpret (l,varMap))))):ts,varMap)
                                                            | op1 == Pow = evaluate (n:(Operator op):(Value (Number ((interpret (m,varMap)) ** (interpret (l,varMap))))):ts,varMap)
evaluate ((n:(Operator op):m:ts),varMap)    | op == Add = evaluate ((Value (Number ((interpret (n,varMap)) + (interpret (m,varMap))))):ts,varMap)
                                            | op == Sub = evaluate ((Value (Number ((interpret (n,varMap)) - (interpret (m,varMap))))):ts,varMap)
                                            | op == Mult = evaluate ((Value (Number ((interpret (n,varMap)) * (interpret (m,varMap))))):ts,varMap)
                                            | op == Div = evaluate ((Value (Number ((interpret (n,varMap)) / (interpret (m,varMap))))):ts,varMap)
                                            | op == Pow = evaluate ((Value (Number ((interpret (n,varMap)) ** (interpret (m,varMap))))):ts,varMap)
evaluate (_,_) = error "(103)Evaluate Error: Incomplete Input"

--output :: a -> String
output :: String -> Map String Double -> (String, Map String Double)
output input varMap = preEvaluate (parse $ lexer input, varMap)
